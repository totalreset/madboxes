# STM32 Dj Midicontroller

## Realizing a minimal dj midicontroller

The project aims to realize a simple dj midi controller open source easy to built and program for a cheap price.
The idea is to have just 8-10 Knobs, which are used for the eq and gain of the two tracks, 2 faders for the volumes and 12 buttons for functions as stop and play, move forward or backward the track and the sync button.

# What is needed?

1x Stm32 black/blue pill which you can find cheap around. \
8-10x Pots, 2x faders and 10-12x push buttons. \
1x Box. (madbox) \
1x Multiplexer ( example 74HC4067 ) \
Soldering material and energies to do it.

# Flashing the software

You can put the arduino bootloader to flash the stm32 with Arduino's IDE.

<p>https://www.etechpath.com/how-to-flash-usb-bootloader-in-stm32-black-pill-board-to-program-it-with-arduino-ide/</p>

# TODO

1 Realizing the hardware design and scheme: wiring, choosing the components and final layout. \
2 Writing down the code compatible with stm32. ( an example for arduino is here : <p>https://git.puscii.nl/totalreset/madboxes/-/tree/master/Multiplexer%20Midi%20Controller%20Arduino%20Example</p> \
3 The realization of the pcb board. \
4 Trying to make it to test it.





/*---------------------------------------------------------------------------------------------

  Open Sound Control (OSC) library for the ESP8266/ESP32

  Example for sending messages from the ESP8266/ESP32 to a remote computer
  The example is sending "hello, osc." to the address "/test".

  This example code is in the public domain.

--------------------------------------------------------------------------------------------- */
#if defined(ESP8266)
#include <ESP8266WiFi.h>
#else
#include <WiFi.h>
#endif
#include <WiFiUdp.h>
#include <OSCMessage.h>

//led seting
#ifndef BUILTIN_LED
#ifdef LED_BUILTIN
#define BUILTIN_LED LED_BUILTIN
#else
#define BUILTIN_LED 13
#endif
#endif

unsigned int transport;              // holds transport message comming from midi 0clock 1start 2stop
const int analogInPin = A0;  // ESP8266 Analog Pin ADC0 = A0

char ssid[] = "AndroidAP";          // your network SSID (name)
char pass[] = "141654tyabc";                    // your network password
WiFiUDP Udp;                                // A UDP instance to let us send and receive packets over UDP
const IPAddress outIp(192,168,43,252);        // remote IP of your computer
const unsigned int outPort = 12345;          // remote port to receive OSC
const unsigned int localPort = 8888;        // local port to listen for OSC packets (actually not used for sending)

int sensorValue = 0;  // value read from the pot
int midi_ch = 0;
int cc = 11;
int ccVal = 0;
int ccValPrev = 0;

int recordedValues[1][96];
int noteNum  = 0;

OSCErrorCode error;

//timing stuff 
unsigned long previousMillis = 0;       
const long pollInterval = 5;   

//midi step stuff
int currentStep = 0; // 0-95 for 4 bars

void setup() {
    Serial.begin(115200);
    pinMode(BUILTIN_LED, OUTPUT);
    digitalWrite(BUILTIN_LED, LOW); 
    // Connect to WiFi network
    Serial.println();
    Serial.println();
    Serial.print("Connecting to ");
    Serial.println(ssid);
    WiFi.begin(ssid, pass);

    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
    }
    Serial.println("");

    Serial.println("WiFi connected");
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());

    Serial.println("Starting UDP");
    Udp.begin(localPort);
    Serial.print("Local port: ");
#ifdef ESP32
    Serial.println(localPort);
#else
    Serial.println(Udp.localPort());
#endif

}


int ledState = LOW;             // ledState used to set the LED
unsigned long previousMillisBlink = 0;        // will store last time LED was update
const long blinkInterval = 20; 

void blink(){
  unsigned long currentMillis = millis();

  if (currentMillis - previousMillisBlink >= blinkInterval) {
    // save the last time you blinked the LED
    previousMillisBlink = currentMillis;

    // if the LED is off turn it on and vice-versa:
    if (ledState == LOW) {
      ledState = HIGH;
    } else {
      ledState = LOW;
    }

    // set the LED with the ledState of the variable:
    digitalWrite(BUILTIN_LED, ledState);
  }
}


void transportCallback(OSCMessage &msg) {
    transport = msg.getInt(0);
//    digitalWrite(BUILTIN_LED, ledState);
//    Serial.print("/transport: ");
//    Serial.println(transport);
//    Serial.println(currentStep);
    if(transport == 1){currentStep=0; noteNum=0;}
    if(transport == 2){currentStep=0; noteNum=0;}

    if(transport == 0){
      if(currentStep % 3 == 0){ // one note 
        if(currentStep % 12 == 0){ // one beat
          Serial.print("beat ");
           blink();
        }
//        Serial.print("note num:");
//        Serial.println(noteNum);
        noteNum++;
        if(noteNum == 16 ) {noteNum = 0;}
      }
//      Serial.print(currentStep);
//      Serial.print("value ");
//      Serial.println(recordedValues[currentStep]);
      sendCcOsc(recordedValues[0][currentStep]);
      currentStep++;
      if(currentStep == 96 ) {currentStep = 0;}

    }
}

void sendCcOsc(int ccVal){
    OSCMessage msg("/cc/recv");
    msg.add(midi_ch);
    msg.add(cc);
    msg.add(ccVal);
    Udp.beginPacket(outIp, outPort);
    msg.send(Udp);
    Udp.endPacket();
    msg.empty();


}

void pollAnalog() {
    sensorValue = analogRead(analogInPin);
    ccVal = map (sensorValue, 0, 1023, 0, 127);
    if( ccVal != ccValPrev){
      sendCcOsc(ccVal);
      recordedValues[0][currentStep] = ccVal;
    }
    ccValPrev=ccVal;
}

void recvOscMsg() {
    OSCMessage msg;
    int size = Udp.parsePacket();

    if (size > 0) {
      while (size--) {
        msg.fill(Udp.read());
      }
      if (!msg.hasError()) {
        msg.dispatch("/transport", transportCallback);
      } else {
        error = msg.getError();
        Serial.print("error: ");
        Serial.println(error);
      }
    }
}

void loop() {
    recvOscMsg();
//    pollAnalog(); 
    unsigned long currentMillis = millis();    
    if (currentMillis - previousMillis >= pollInterval) {
        // save the last time you blinked the LED
        previousMillis = currentMillis;
        pollAnalog();

    }
}

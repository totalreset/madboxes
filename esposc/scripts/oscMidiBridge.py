#!/bin/python3

import liblo, sys
import mido 
#import rtmidi

midi_out = mido.open_output('ESP', virtual=True)
midi_in = mido.open_input('clock', virtual=True)

def cc_recv_callback(path, args):
    ch, cc, val = args
    print("/cc/recv: received message '%s' ch: '%d' cc: '%d' val: '%d'" % (path, ch, cc, val))
    midi_msg = mido.Message('control_change', channel=ch, control=cc, value=val)
#    midi_msg = mido.Message('pitchwheel', channel=ch, pitch=val)

    midi_out.send(midi_msg)

def note_recv_callback(path, args):
    ch, note = args
    print("/note/recv: received message '%s' ch: '%d' note: '%d' " % (path, ch, note))


def fallback(path, args, types, src):
    print("got unknown message '%s' from '%s'" % (path, src.url))
    for a, t in zip(args, types):
        print("argument of type '%s': %s" % (t, a))

def midi_in_callback(message):
#    print(message.hex())
    if(message.hex() == 'F8'):
#        print('clocktick');
        liblo.send(target, "/transport", 0)
    if(message.hex() == 'FC'):
        print('transport stop');
        liblo.send(target, "/transport", 1)
    if(message.hex() == 'FA'):
        print('transport start');
        liblo.send(target, "/transport", 2)


# create osc server, listening on port 1234
try:
    server = liblo.ServerThread(12345)
except liblo.ServerError as err:
    print(err)
    sys.exit()
# register method taking an int and a float
server.add_method("/cc/recv", 'iii', cc_recv_callback)
server.add_method("/note/recv", 'ii', cc_recv_callback)

# connect to the esp
try:
    target = liblo.Address('10.205.10.181',8888)
except liblo.AddressError as err:
    print(err)
    sys.exit()

# register a fallback for unhandled messages
server.add_method(None, None, fallback)
server.start() 

# set midi clock callback i
midi_in.callback = midi_in_callback

input("ctrl-c to exit")

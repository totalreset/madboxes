# ESPOSC osc running on an esp

Run OSC (https://en.wikipedia.org/wiki/Open_Sound_Control)  on an ESP8266 (https://en.wikipedia.org/wiki/ESP8266)  to have it interface with a bunch of midi devices connected to a computer. 
some wireless controller, maybe with some motion sensors or something, so you can jump around the room modulating your synths.
maybe put it in a tennis ball to throw it around.

# TODO

- figure out which osc lib to use, there is a few. Needs to be simple to use and as low latency as possible. 
- write some simple osc server software where the esps can connect to, interfacing it to midi (https://pypi.org/project/python-osc/ https://github.com/louisabraham/python3-midi http://das.nasophon.de/mididings/ )
- hardware for the "server" maybe a raspbery with a wifi stick and and a midi interface?

# DONE

- made this file
- some basic functionality

# FILES

scripts/oscMidiBridge.py - uses mido and liblo to convert incoming OSC messages to midi control change messages
esp/ESP8266sendCC/ESP8266sendCC.ino - arduino sketch for esp. uses https://github.com/CNMAT/OSC. for now it just sends the analog input over osc

# PREREQUIRTITIES
for the oscmidibridge script some python libs are required:
apt-get install python3-rtmidi python3-liblo python3-mid

for the hardware shizle:
current prototype is a wemos d1 mini esp board connected to a ADXL345 accelerometer via I2C, a battery charher and voltage regulator board, and a li-ion cell.

you need a modern version of the arduino IDE (confirmed with 1.8.13) https://www.arduino.cc/en/software
instal the ESP8266 support from the board manager within the arduino IDE
you also need to install some libraries form the library manager in the ide: "Accelerometer ADXL345", "Adafruit Unified Sensors" "ArdOSC" and probably some im forgetting.

int Pot1 = 0; // Define pots
int lastPot1 = 0;
int Pot2 = 0;
int lastPot2 = 0;
int Pot3 = 0;
int lastPot3 = 0;
int Pot4 = 0;
int lastPot4 = 0;
int Pot5 = 0;
int lastPot5 = 0;
int Pot6 = 0;
int lastPot6 = 0;


void setup()
{
   Serial.begin(31250);       // 31250 for MIDI port and 9600 for usb serial to midi for middleware
}

void loop()
{
  
   Pot1 = analogRead(0)/8;   // Divide by 8 to get range of 0-127 for midi
   if (Pot1 != lastPot1) // If the value does not = the last value the following command is made. This is because the pot has been turned. Otherwise the pot remains the same and no midi message is output.
   {
   MIDImessage(176,74,Pot1);}   // 176 = CC command (channel 1 control change), 1 = Which Control, Pot = value read from Potentionmeter 1. CHECK MIDI TABLE for info
   lastPot1 = Pot1;

   Pot2 = analogRead(1)/8;   // Divide by 8 to get range of 0-127 for midi
   if (Pot2 != lastPot2) 
   {
   MIDImessage(180,12,Pot2);}   // 180 = CC command (channel 2 control change), 2 = Which Control, Pot2 = value read from Potentionmeter 2
   lastPot2 = Pot2;
   
   Pot3 = analogRead(2)/8;   
   if (Pot3 != lastPot3) 
   {
   MIDImessage(181,13,Pot3);}   
   lastPot3 = Pot3;   

   Pot4 = analogRead(3)/8;   
   if (Pot4 != lastPot4) 
   {
   MIDImessage(182,14,Pot4);}   
   lastPot4 = Pot4;

   Pot5 = analogRead(4)/8;   
   if (Pot5 != lastPot5) 
   {
   MIDImessage(183,15,Pot5);}   
   lastPot5 = Pot5;

   Pot6 = analogRead(5)/8;   
   if (Pot6 != lastPot6) 
   {
   MIDImessage(184,16,Pot6);}         
   lastPot6 = Pot6;

 delay(10);  // Helps against fluctuations
}

void MIDImessage(byte command, byte data1, byte data2) //pass values out through standard Midi Command
{
   Serial.write(command);
   Serial.write(data1);
   Serial.write(data2);
}


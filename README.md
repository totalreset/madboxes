# Madboxes

## What is it?

Arduino midicontroller/diy synthetizer or any kind of wierd boxes, does not have to be music related.

## What is in here?

A collection of codes, documentation and other material for various projects regarding this event.
Could be Arduino's code, Fritzing project, pictures...

## Upload your projects!

If you feel like you are having a project or you want to start it and you like madboxes, you can upload your code here and share it to develop toghether.
This repo should be editable, if not just clone it and upload it on another Git instance, or ask someone to make you an account on puscii.nl.

